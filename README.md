# PSQL training

This is a small project to try out SQL :)

Init database in your container with :
    
    psql -U postgres < /data/initbase.sql
    
Here you go :

```sql
SELECT * FROM books
LEFT JOIN authors a on books.author_id = a.id;

SELECT * FROM books
LEFT JOIN libraries_books lb on books.id = lb.book_id
LEFT JOIN libraries l on lb.library_id = l.id;

SELECT * FROM books
INNER JOIN libraries_books lb on books.id = lb.book_id
INNER JOIN libraries l on lb.library_id = l.id;

SELECT * FROM books
FULL OUTER JOIN libraries_books lb on books.id = lb.book_id
FULL OUTER JOIN libraries l on lb.library_id = l.id
FULL OUTER JOIN authors a on books.author_id = a.id;

SELECT * FROM libraries
LEFT JOIN libraries_books lb on libraries.id = lb.library_id
WHERE libraries.name = 'Furet';

SELECT * FROM libraries
LEFT JOIN libraries_books lb on libraries.id = lb.library_id
LEFT JOIN books b on lb.book_id = b.id
WHERE libraries.name = 'Furet';
```