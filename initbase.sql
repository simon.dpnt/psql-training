/** TABLES **/
create table authors
(
    id int
        constraint authors_pk
            primary key,
    name varchar(255) not null,
    nationality varchar(255) default NULL
);

create table books
(
    id int
        constraint books_pk
            primary key,
    title varchar(255) not null,
    author_id int REFERENCES authors(id),
    editor varchar(255) default null
);

create table libraries
(
    id int
        constraint libraries_pk
            primary key,
    name varchar(255) not null
);

create table libraries_books
(
    library_id int REFERENCES libraries(id),
    book_id int REFERENCES books(id)
);

/** INSERTS **/
INSERT INTO public.authors (id, name, nationality) VALUES (1, 'Tolkien', 'English');
INSERT INTO public.authors (id, name, nationality) VALUES (3, 'Crichton', 'American');
INSERT INTO public.authors (id, name, nationality) VALUES (4, 'Fabcaro', 'French');
INSERT INTO public.authors (id, name, nationality) VALUES (5, 'Astier', 'French');
INSERT INTO public.authors (id, name, nationality) VALUES (2, 'Watterson', 'American');

INSERT INTO public.books (id, title, author_id, editor) VALUES (1, 'Lord Of The Rings', 1, 'EDITHOR');
INSERT INTO public.books (id, title, author_id, editor) VALUES (2, 'Bilbo', 1, 'OTHEREDITOR');
INSERT INTO public.books (id, title, author_id, editor) VALUES (3, 'Calvin & Hobbes T1', 2, 'OTHEREDITOR');
INSERT INTO public.books (id, title, author_id, editor) VALUES (4, 'Calvin & Hobbes T2', 2, 'OTHEREDITOR');
INSERT INTO public.books (id, title, author_id, editor) VALUES (5, 'Jurassic Park', 3, 'EDITHOR');
INSERT INTO public.books (id, title, author_id, editor) VALUES (6, 'Timeline', 3, 'SFEDIT');
INSERT INTO public.books (id, title, author_id, editor) VALUES (7, 'CONversations', 4, 'FrenchEdition');
INSERT INTO public.books (id, title, author_id, editor) VALUES (8, 'ZaïZaïZaïZaï', 4, 'Cédrole');
INSERT INTO public.books (id, title, author_id, editor) VALUES (9, 'Kaamelott T1', 5, 'Cédrole');
INSERT INTO public.books (id, title, author_id, editor) VALUES (10, 'L''ExoBook', 5, 'Cédrole');

INSERT INTO public.libraries (id, name) VALUES (1, 'Furet');
INSERT INTO public.libraries (id, name) VALUES (2, 'Fnac');
INSERT INTO public.libraries (id, name) VALUES (3, 'Bateau Livre');
INSERT INTO public.libraries (id, name) VALUES (4, 'Passion Culture');

INSERT INTO public.libraries_books (library_id, book_id) VALUES (1, 1);
INSERT INTO public.libraries_books (library_id, book_id) VALUES (2, 1);
INSERT INTO public.libraries_books (library_id, book_id) VALUES (3, 2);
INSERT INTO public.libraries_books (library_id, book_id) VALUES (4, 2);
INSERT INTO public.libraries_books (library_id, book_id) VALUES (1, 3);
INSERT INTO public.libraries_books (library_id, book_id) VALUES (1, 4);
INSERT INTO public.libraries_books (library_id, book_id) VALUES (3, 4);
INSERT INTO public.libraries_books (library_id, book_id) VALUES (4, 6);
INSERT INTO public.libraries_books (library_id, book_id) VALUES (2, 6);
INSERT INTO public.libraries_books (library_id, book_id) VALUES (3, 6);
INSERT INTO public.libraries_books (library_id, book_id) VALUES (4, 7);
INSERT INTO public.libraries_books (library_id, book_id) VALUES (3, 8);
INSERT INTO public.libraries_books (library_id, book_id) VALUES (4, 9);
INSERT INTO public.libraries_books (library_id, book_id) VALUES (2, 9);
INSERT INTO public.libraries_books (library_id, book_id) VALUES (1, 10);